----------------------------------------------------
NSH Barcode validator v1.5.0
Scan time : 2017-01-25 11:12:12
Writing barcode binary to: C:\NSH Gate\tmp\data.barcode

First byte is skipped (barcode reader prepends a byte before barcode data)
DST ticket
Start date: 2017-01-05
End date  : 2017-01-05
Ticket  expired 20 day(s) 11:12 hours ago
Signature key:1184NS002
ScanTime          : 2017-01-25 11:12:12
SignatureValidated: False
DateValidated     : False

Ticket: 
+-------------------+-------+
| Key               | Value |
+-------------------+-------+
| Ticket ID         |       |
| Type ID           | #UT   |
| Type version      | 01    |
| Rics Code         | 1184  |
| Signature version | NS002 |
+-------------------+-------+

U_HEAD: U_HEAD0100531184          8369400.000501201709286NLEN
+-----------------+--------------+
| Key             | Value        |
+-----------------+--------------+
| Record ID       | U_HEAD       |
| Record Version  | 01           |
| HeaderLength    | 53           |
| RiscCode        | 1184         |
| TicketId        | 8369400.00   |
| EditionTime     | 050120170928 |
| Flags           | 6            |
| Language        | NL           |
| Second Language | EN           |
+-----------------+--------------+

U_TLAY: U_TLAY010430RCT20020
+------------------+--------+
| Key              | Value  |
+------------------+--------+
| Record ID        | U_TLAY |
| Record version   | 01     |
| Record length    | 430    |
| Layout standard  | RCT2   |
| Number of fields | 0020   |
+------------------+--------+
Fields:0019013200019Vervoerbewijs Enkel01200106000000202010300003CIV020601040000411840053012000011Hollander V0153012000001102530120000000302015000035VALID FROM 05/01/2017 TO 05/01/20170614013000010Amersfoort0635013000005Baarn0714013000005*****0735013000005*****066801010000120802010300003VIA0806019900019Amersfoort%NS%Baarn1357010300003EUR13610108000042.501405013200011Enkele reis1457010300003***1461010800008********
+------+------+--------+--------+-------+------------+--------+-------------------------------------+
| [Nr] | Line | Column | Height | Width | Formatting | Length | Text                                |
+------+------+--------+--------+-------+------------+--------+-------------------------------------+
| 1    | 0    | 19     | 1      | 32    | 0          | 19     | Vervoerbewijs Enkel                 |
| 2    | 1    | 20     | 1      | 6     | 0          | 0      |                                     |
| 3    | 2    | 2      | 1      | 3     | 0          | 3      | CIV                                 |
| 4    | 2    | 6      | 1      | 4     | 0          | 4      | 1184                                |
| 5    | 0    | 53     | 1      | 20    | 0          | 11     | Hollander V                         |
| 6    | 1    | 53     | 1      | 20    | 0          | 1      | 1                                   |
| 7    | 2    | 53     | 1      | 20    | 0          | 0      |                                     |
| 8    | 3    | 2      | 1      | 50    | 0          | 35     | VALID FROM 05/01/2017 TO 05/01/2017 |
| 9    | 6    | 14     | 1      | 30    | 0          | 10     | Amersfoort                          |
| 10   | 6    | 35     | 1      | 30    | 0          | 5      | Baarn                               |
| 11   | 7    | 14     | 1      | 30    | 0          | 5      | *****                               |
| 12   | 7    | 35     | 1      | 30    | 0          | 5      | *****                               |
| 13   | 6    | 68     | 1      | 1     | 0          | 1      | 2                                   |
| 14   | 8    | 2      | 1      | 3     | 0          | 3      | VIA                                 |
| 15   | 8    | 6      | 1      | 99    | 0          | 19     | Amersfoort%NS%Baarn                 |
| 16   | 13   | 57     | 1      | 3     | 0          | 3      | EUR                                 |
| 17   | 13   | 61     | 1      | 8     | 0          | 4      | 2.50                                |
| 18   | 14   | 5      | 1      | 32    | 0          | 11     | Enkele reis                         |
| 19   | 14   | 57     | 1      | 3     | 0          | 3      | ***                                 |
| 20   | 14   | 61     | 1      | 8     | 0          | 8      | ********                            |
+------+------+--------+--------+-------+------------+--------+-------------------------------------+

Layout:
              1         2         3         4         5         6         
    0123456789012345678901234567890123456789012345678901234567890123456789
    ----------------------------------------------------------------------
  0|                   Vervoerbewijs Enkel               Hollander V      |
  1|                                                     1                |
  2|  CIV 1184                                                            |
  3|  VALID FROM 05/01/2017 TO 05/01/2017                                 |
  4|                                                                      |
  5|                                                                      |
  6|              Amersfoort           Baarn                            2 |
  7|              *****                *****                              |
  8|  VIA Amersfoort%NS%Baarn                                             |
  9|                                                                      |
 10|                                                                      |
 11|                                                                      |
 12|                                                                      |
 13|                                                         EUR 2.50     |
 14|     Enkele reis                                         *** ******** |
    ----------------------------------------------------------------------

Errors:
- Travel dates are not valid
- Certificate for the ticket could not be found

