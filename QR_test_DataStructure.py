# todo: change order of function calls to enable calculating correct record lengths

import random
import treepoem
import datetime
import collections
import itertools
import zlib
import base64


def random_bytes(n):
    return bytes(random.getrandbits(8) for i in range(n))

TODAY = datetime.datetime.today().strftime('%d%m%Y%H%M')
RICS_CODE_CXX = str(9999).zfill(4)
RECORD_VERSION = str(1).zfill(2)
PROD_CLASS = 2
MESS_ID = '#UT'
TYPE_VERSION = '01'
SIGNATURE_ID = 'CXX01'
SIGNATURE = b'\x00'*50  # Signature is mocked as 50 null bytes


def create_ticket_type_header(rics_code, message_type_id, signature_id):
    ticket_type_header_dict = collections.OrderedDict([
        ('Ticket ID', ''),
        ('Type ID', message_type_id),
        ('Type Version', str(1).zfill(2)),
        ('RICS Code', rics_code),
        ('Signature Version', signature_id),
    ])
    return ''.join([value for value in ticket_type_header_dict.values()])


def create_ticket_header(rics_code, record_version):
    ticket_header_dict = collections.OrderedDict([
        ('Record ID', 'U_HEAD'),
        ('Record Version', record_version),
        ('Record Length', str(53).zfill(4)),  # todo dit moet berekende waarde zijn! Of altijd 53?
        ('RICS Code', rics_code),
        ('TicketId', '{0: >20}'.format(123456.78)),
        ('Editiontime', TODAY),
        ('Flags', str(6)),
        ('Language', 'NL'),
        ('Second Language', 'EN'),
    ])
    return ''.join([value for value in ticket_header_dict.values()])


def create_ticket_layout(record_version):
    ticket_layout_dict = collections.OrderedDict([
        ('Record ID', 'U_TLAY'),
        ('Record Version', record_version),
        ('Record length', str(463).zfill(4)),  # todo: this must be calculated field: len(tlay + barcodes)
        ('Layout standard', 'RCT2'),
        ('Number of fields', '0020'),  # todo: this must be calculated field: len(data_fields)
    ])
    return ''.join([value for value in ticket_layout_dict.values()])


def create_ticket_data(rics_code, today=TODAY, prod_class='2'):
    nr1_product_name = 'Vervoerbewijs Enkel'
    nr2_empty = ''
    nr3_CIV = 'CIV'  # todo: leeg laten als er geen CIV code is
    nr4_RICS = rics_code  # todo: leeg laten als er geen CIV code is
    nr5_name = 'Hollander V'
    nr6_nrAdults = '1'
    nr7_nrChildren = ''
    nr8_Validity = 'VALID FROM %s TO %s' % (today, today)
    nr9_departStopAway = 'Assen'  # todo: this must be 'middel-lange naam'
    nr10_arrivalStopAway = 'Maastricht'
    nr11_departStopReturn = '*****'
    nr12_arrivalStopReturn = '*****'
    nr13_class = prod_class
    nr14_via = 'VIA'
    nr15_journeySummary = 'Assen%NS%Nijmegen%Veolia%Roermond%NS%Maastricht'
    nr16_currency = 'EUR'
    nr17_price = '26.59'
    nr18_propositionName = 'Enkele reis'
    nr19_fixed = '***'
    nr20_fixed = '********'

    ticket_data_list = [
        [1, 0, 19, 1, 32, 0, len(nr1_product_name), nr1_product_name],
        [2, 1, 20, 1, 6, 0, len(nr2_empty), nr2_empty],
        [3, 2, 2, 1, 3, 0, len(nr3_CIV), nr3_CIV],
        [4, 2, 6, 1, 4, 0, len(nr4_RICS), nr4_RICS],
        [5, 0, 53, 1, 20, 0, len(nr5_name), nr5_name],
        [6, 1, 53, 1, 20, 0, len(nr6_nrAdults), nr6_nrAdults],
        [7, 2, 53, 1, 20, 0, len(nr7_nrChildren), nr7_nrChildren],
        [8, 3, 2, 1, 50, 0, len(nr8_Validity), nr8_Validity],
        [9, 6, 14, 1, 30, 0, len(nr9_departStopAway), nr9_departStopAway],
        [10, 6, 35, 1, 30, 0, len(nr10_arrivalStopAway), nr10_arrivalStopAway],
        [11, 7, 14, 1, 30, 0, len(nr11_departStopReturn), nr11_departStopReturn],
        [12, 7, 35, 1, 30, 0, len(nr12_arrivalStopReturn), nr12_arrivalStopReturn],
        [13, 6, 68, 1, 1, 0, len(nr13_class), nr13_class],
        [14, 8, 2, 1, 3, 0, len(nr14_via), nr14_via],
        [15, 8, 6, 1, 99, 0, len(nr15_journeySummary), nr15_journeySummary],
        [16, 13, 57, 1, 3, 0, len(nr16_currency), nr16_currency],
        [17, 13, 61, 1, 8, 0, len(nr17_price), nr17_price],
        [18, 14, 5, 1, 32, 0, len(nr18_propositionName), nr18_propositionName],
        [19, 14, 57, 1, 3, 0, len(nr19_fixed), nr19_fixed],
        [20, 14, 61, 1, 8, 0, len(nr20_fixed), nr20_fixed],
    ]

    ticket_data_list_formatted = []
    for i in range(len(ticket_data_list)):
        ticket_data_list_formatted.append([''.join([
            str(ticket_data_list[i][1]).zfill(2),
            str(ticket_data_list[i][2]).zfill(2),
            str(ticket_data_list[i][3]).zfill(2),
            str(ticket_data_list[i][4]).zfill(2),
            str(ticket_data_list[i][5]).zfill(1),
            str(ticket_data_list[i][6]).zfill(4),
            ticket_data_list[i][7]
        ])])

    return ''.join(itertools.chain.from_iterable(ticket_data_list_formatted))


def compress_message(message):
    print(message)
    return zlib.compress(message.encode('utf-8'))


def create_dst(message_type_id, type_version, rics_code, signature_id, signature, compressed_message):
    dst_dict = collections.OrderedDict([
        ('Message type ID', message_type_id),
        ('Type Version', type_version),
        ('RICS Code', rics_code),
        ('Signature ID', signature_id),
        ('Signature', str(signature)),
        ('Length of compressed message', str(len(compressed_message)).zfill(4)),
        ('Compressed message', str(compressed_message))
    ])

    return ''.join([value for value in dst_dict.values()])


def create_barcode(data):
    print('DST Data-string: ', data)
    data_b64 = base64.b64encode(data.encode('utf-8'))
    print('DST Data-string (b64): ', data_b64)
    return treepoem.generate_barcode(
        barcode_type='azteccode',
        data=data,
        options={'format': 'full',
                 # 'layers': 17,
                 }
    )


ticket_type_header_str = create_ticket_type_header(RICS_CODE_CXX, MESS_ID, SIGNATURE_ID)
ticket_header_str = create_ticket_header(RICS_CODE_CXX, RECORD_VERSION)
ticket_layout_str = create_ticket_layout(RECORD_VERSION)
ticket_data_list_form = create_ticket_data(RICS_CODE_CXX)
message_str = ticket_type_header_str + ticket_header_str + ticket_layout_str + ticket_data_list_form

print(message_str)

message_compressed_bin = compress_message(message_str)

dst_message_str = create_dst(MESS_ID, TYPE_VERSION, RICS_CODE_CXX, SIGNATURE_ID, SIGNATURE, message_compressed_bin)

barcode_img = create_barcode(dst_message_str)
barcode_img.save('barcodes/barcode_%s.png' % TODAY)


# Checks:
print('Header length: ', len(ticket_layout_str))
print('Layout+fields length: ', len(ticket_layout_str+ticket_data_list_form))


# DST Data-string:  #UT019999CXX01b'\x10\xbf\x15\xe4 @\xcck\x9b\x86\xa6b\x94\x87\xed\x159\xef\xe0\xdd\xbf3\x05\xbb\x99\x94\xb35!\xe8\xcc/Z;\x18\x1b8\xbd\xf6\xb0\x95\x06\x95\xcf\xddi\x07\xa5\xe7\x1c'0301b'x\x9cUQ\xc1n\x830\x0c\xfd\x95HS/=TvB\x02="\xcaT$J%\x06\xa8;Ul\x8d6:\n\x12T\xdb\xef\xcf\xce\xda\xc2^r\xb0\x9d\xbc\xe7\x17\xe7\xa9,\x00\xd7\x84\xe8p\x00,\x8f\xdb8\xdc\x00\x02h\xc5E1\x01\xa5\xf2\xb4Y\xf9\x81T\x80\x12\xd0G\x83\xcadi\x9c\x95\xc7"\r_\x89\xe4\x19\x95G\x85\x04\xa0M\xa2\x80\xea/\xa8\xec\xf0\xdd\xdb\xe1\xcd\xfe4\xe7Q\xc4\xdd\x97mY\x82\x18\x06\x1c$-\x04\xc5\xa1\x8a\x92\x8a2\xc3r\x9c{l\x83\xed8\x06qp\xdb\xb7m\xdd\x9d\xec *\xc0G\x9d\x0e@N\x19\t\xb1\xa6v\xd1\xba\n\xd3d#\x9e\xf3\xfdN\xcc\xdd\x8bb\xff/\x07\x83\x1e\x99vt\x1d\x8e\xa3\xed\xc0(}\xab \xec\xeaz\xbc\x0e\xcd\xfb\xe7\x15\xfc\xd9\xc5%\x03\xfc\xc7\xc5{\xc5\x98\x80H\xe8\xb8\x12\x82\xd9\x13\xab$\xa4\xdc\xf0\xdc\xf9\x85\xbek\xb5\xc8^\x16Ys\xbe\xd8\x0f\x8a+\xdb\xb7M\xbd\xc8ij\x97\xbe;\xf1\xd9\xd4\x1d\x95\xf6\x1fZq\x99\xd37p\x9f\xc0\xf5\x96f\xa5\xd7\xe4N\xdf\xa7\x8fn\xdeV\x0c\xb6\x19\xd1\x9b1\xc9#z\x133X\xde\xf0\x0b\x1c\xa2} '
