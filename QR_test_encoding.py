# todo: change order of function calls to enable calculating correct record lengths

import random
import treepoem
import datetime
import collections
import itertools
import zlib
import base64


def random_bytes(n):
    return bytes(random.getrandbits(8) for i in range(n))

TODAY = datetime.datetime.today().strftime('%d%m%Y%H%M')
RICS_CODE_CXX = str(9999).zfill(4)
RECORD_VERSION = str(1).zfill(2)
PROD_CLASS = 2
MESS_ID = '#UT'
TYPE_VERSION = '01'
SIGNATURE_ID = 'CXX01'
SIGNATURE = b'\x00'*50  # Signature is mocked as 50 null bytes


def create_ticket_type_header(message_type_id, type_version, rics_code, signature_id):
    ticket_type_header_dict = collections.OrderedDict([
        ('Ticket ID', ''),
        ('Type ID', message_type_id),
        ('Type Version', type_version),
        ('RICS Code', rics_code),
        ('Signature Version', signature_id),
    ])
    return (''.join([value for value in ticket_type_header_dict.values()])).encode('utf-8')


def create_ticket_header(rics_code, record_version):
    ticket_header_dict = collections.OrderedDict([
        ('Record ID', 'U_HEAD'),
        ('Record Version', record_version),
        ('Record Length', str(53).zfill(4)),  # todo dit moet berekende waarde zijn! Of altijd 53?
        ('RICS Code', rics_code),
        ('TicketId', '{0: >20}'.format(123456.78)),
        ('Editiontime', TODAY),
        ('Flags', '6'),
        ('Language', 'NL'),
        ('Second Language', 'EN'),
    ])
    return (''.join([value for value in ticket_header_dict.values()])).encode('utf-8')


def create_ticket_data(rics_code, today=TODAY, prod_class='2'):
    nr1_product_name = 'Vervoerbewijs Enkel'
    nr2_empty = ''
    nr3_CIV = 'CIV'  # todo: leeg laten als er geen CIV code is
    nr4_RICS = rics_code  # todo: leeg laten als er geen CIV code is
    nr5_name = 'Hollander V'
    nr6_nrAdults = '1'
    nr7_nrChildren = ''
    nr8_Validity = 'VALID FROM %s TO %s' % (today, today)
    nr9_departStopAway = 'Assen'  # todo: this must be 'middel-lange naam'
    nr10_arrivalStopAway = 'Maastricht'
    nr11_departStopReturn = '*****'
    nr12_arrivalStopReturn = '*****'
    nr13_class = prod_class
    nr14_via = 'VIA'
    nr15_journeySummary = 'Assen%NS%Nijmegen%Veolia%Roermond%NS%Maastricht'
    nr16_currency = 'EUR'
    nr17_price = '26.59'
    nr18_propositionName = 'Enkele reis'
    nr19_fixed = '***'
    nr20_fixed = '********'

    ticket_data_list = [
        [1, 0, 19, 1, 32, 0, len(nr1_product_name), nr1_product_name],
        [2, 1, 20, 1, 6, 0, len(nr2_empty), nr2_empty],
        [3, 2, 2, 1, 3, 0, len(nr3_CIV), nr3_CIV],
        [4, 2, 6, 1, 4, 0, len(nr4_RICS), nr4_RICS],
        [5, 0, 53, 1, 20, 0, len(nr5_name), nr5_name],
        [6, 1, 53, 1, 20, 0, len(nr6_nrAdults), nr6_nrAdults],
        [7, 2, 53, 1, 20, 0, len(nr7_nrChildren), nr7_nrChildren],
        [8, 3, 2, 1, 50, 0, len(nr8_Validity), nr8_Validity],
        [9, 6, 14, 1, 30, 0, len(nr9_departStopAway), nr9_departStopAway],
        [10, 6, 35, 1, 30, 0, len(nr10_arrivalStopAway), nr10_arrivalStopAway],
        [11, 7, 14, 1, 30, 0, len(nr11_departStopReturn), nr11_departStopReturn],
        [12, 7, 35, 1, 30, 0, len(nr12_arrivalStopReturn), nr12_arrivalStopReturn],
        [13, 6, 68, 1, 1, 0, len(nr13_class), nr13_class],
        [14, 8, 2, 1, 3, 0, len(nr14_via), nr14_via],
        [15, 8, 6, 1, 99, 0, len(nr15_journeySummary), nr15_journeySummary],
        [16, 13, 57, 1, 3, 0, len(nr16_currency), nr16_currency],
        [17, 13, 61, 1, 8, 0, len(nr17_price), nr17_price],
        [18, 14, 5, 1, 32, 0, len(nr18_propositionName), nr18_propositionName],
        [19, 14, 57, 1, 3, 0, len(nr19_fixed), nr19_fixed],
        [20, 14, 61, 1, 8, 0, len(nr20_fixed), nr20_fixed],
    ]

    ticket_data_list_formatted = []
    for i in range(len(ticket_data_list)):
        ticket_data_list_formatted.append([''.join([
            str(ticket_data_list[i][1]).zfill(2),
            str(ticket_data_list[i][2]).zfill(2),
            str(ticket_data_list[i][3]).zfill(2),
            str(ticket_data_list[i][4]).zfill(2),
            str(ticket_data_list[i][5]).zfill(1),
            str(ticket_data_list[i][6]).zfill(4),
            ticket_data_list[i][7]
        ])])

    return (''.join(itertools.chain.from_iterable(ticket_data_list_formatted))).encode('utf-8')


def create_ticket_layout(record_version):
    ticket_layout_dict = collections.OrderedDict([
        ('Record ID', 'U_TLAY'),
        ('Record Version', record_version),
        ('Record length', str(463).zfill(4)),  # todo: this must be calculated field: len(tlay + barcodes)
        ('Layout standard', 'RCT2'),
        ('Number of fields', '0020'),  # todo: this must be calculated field: len(data_fields)
    ])
    return (''.join([value for value in ticket_layout_dict.values()])).encode('utf-8')


def compress_message(record):
    return zlib.compress(record)


def create_dst(message_type_id, type_version, rics_code, signature_id, signature, record_version):

    type_header_record = create_ticket_type_header(message_type_id, type_version, rics_code, signature_id)
    header_record = create_ticket_header(rics_code, record_version)
    data_record = create_ticket_data(rics_code)
    layout_record = create_ticket_layout(record_version)

    record_sequence = type_header_record + header_record + layout_record + data_record

    print('Record sequence: ', record_sequence)

    compressed_record_sequence = compress_message(record_sequence)

    dst_dict = collections.OrderedDict([
        ('Message type ID', message_type_id.encode('utf-8')),
        ('Type Version', type_version.encode('utf-8')),
        ('RICS Code', rics_code.encode('utf-8')),
        ('Signature ID', signature_id.encode('utf-8')),
        ('Signature', signature),
        ('Length of compressed message', str(len(compressed_record_sequence)).zfill(4).encode('utf-8')),
        ('Compressed message', compressed_record_sequence),
    ])
    dst_bytes = bytearray()
    for value in dst_dict.values():
        dst_bytes.extend(value)
    return dst_bytes


def create_barcode(dst_record):
    print('DST Data-string: ', dst_record)
    data = str(dst_record)
    # barcodes = dst_record
    print(data)
    return treepoem.generate_barcode(
        barcode_type='azteccode',
        data=data,
        options={'format': 'full',
                 # 'layers': 17,
                 }
    )


dst_record_full = create_dst(MESS_ID, TYPE_VERSION, RICS_CODE_CXX, SIGNATURE_ID, SIGNATURE, RECORD_VERSION)

barcode_img = create_barcode(dst_record_full)

barcode_img.save('barcodes/barcode_%s.png' % TODAY)

