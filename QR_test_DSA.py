from pydsa import dsa
import hashlib
import sys
sys.path.append("/home/vincent/Projects/firefly/pydsa")


# DSA key, 1024 bits prime
dsa_key = {
    "Q": 1461461359677056032138425664688969714401096527653,
    "P": 113003610536769662365475438074349202902393371149098932488829763899759693942182221311951893491037065838678290591836787867236266829425427477322203921585701270997375076009060429934105831431797790713235693561718253840225010037389994367689434248899226231330475152082648849936270434981210830874017521600353881618277,
    "G": 96504423597250666602463350548382591669983630413397284533161601799828504875913402437338367980529992940898864793759282567968196860849581229764805627921115713088555922323634319263032762806965222542087676328725218634401760700374749451348066585982534624077588633442696948741889609514070233035695255374063221721717,
    "pub": 108995193903934240798564100451045627210748695124974357268916707208528553000170266840505861201457239168618690453605371832512801286701989019522907305372175334396049194915209672515965212302226018817730175446251842830527761730120646366217675171871247849970396570237026190763013764161366386690833417893072059869763,
    "priv": 936678825459923885095535567029229102235556154286
}


text = """lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no
sea takimata sanctus est Lorem ipsum dolor sit amet."""

m = hashlib.sha1()
m.update(text.encode('utf-8'))
h = m.hexdigest()

message = int("0x" + m.hexdigest(), 0)
sig = dsa.dsa_sign(dsa_key["Q"], dsa_key["P"], dsa_key["G"], dsa_key["priv"], message)
print("=" * 80)
print("DSA SIGNATURE EXAMPLE")
print("=" * 80)
print("DSA Keypair:")
for k in dsa_key.keys():
    print(k, ':', str(dsa_key[k]))
print("-" * 80)
print("Text:")
print(text)
print("-" * 80)
print("SHA-1:")
print(message)
print("-" * 80)
print("DSA Signature:")
print(sig)
print("-" * 80)
print("Verify:")
print(dsa.dsa_verify(sig[0], sig[1], dsa_key["G"], dsa_key["P"], dsa_key["Q"], dsa_key["pub"], message))
print("-" * 80)